<?php

namespace App\Http\Controllers\Carts;

use App\Http\Requests\Carts\CartRequest;
use App\Http\Resources\Carts\CartResource;
use App\Interfaces\Carts\CartInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class CartController
 * @package App\Http\Controllers\Carts
 * @property-read CartInterface $interface
 */
class CartController extends Controller
{

    protected $routeIndex = 'carts';

    protected $pageTitle = 'Cart';
    protected $createRoute = 'carts.create';

    protected $viewIndex = 'carts.index';
    protected $viewCreate = 'carts.create';
    protected $viewEdit = 'carts.edit';
    protected $viewShow = 'carts.show';

    /**
     * CartController constructor.
     * @param CartInterface $interface
     * @param CartRequest $request
     * @param CartResource $resource
     */
    public function __construct(CartInterface $interface, CartRequest $request)
    {
        parent::__construct($interface, $request, new  CartResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return parent::store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }
}
