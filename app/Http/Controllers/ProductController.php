<?php

namespace App\Http\Controllers;

use App\Http\Resources\Products\BestsellerResource;
use App\Http\Resources\Products\ProductResource;
use App\Interfaces\Products\BestsellerInterface;
use App\Interfaces\Products\ProductInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $limit = 12;

    /**
     * @var BestsellerInterface
     */
    private $bestseller;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var ProductInterface
     */
    private $product;

    /**
     * ProductController constructor.
     * @param ProductInterface $product
     * @param BestsellerInterface $bestseller
     * @param Request $request
     */
    public function __construct(ProductInterface $product, BestsellerInterface $bestseller, Request $request)
    {
        $this->product = $product;
        $this->bestseller = $bestseller;
        $this->request = $request;
    }

    public function all()
    {
        return $this->response($data = $this->product->simplePaginate($this->limit, $this->request->all()), ProductResource::collection($data));
    }

    public function latest()
    {
        return $this->response($data = $this->product->simplePaginate(3, array_merge(['order' => 'created_at','direction' => 'desc'],$this->request->all())), ProductResource::collection($data));
    }

    public function bestSelling()
    {
        return $this->response($data = $this->bestseller->simplePaginate($this->limit, array_merge(['order' => 'created_at','direction' => 'desc'],$this->request->all())), BestsellerResource::collection($data));
    }

    private function response($data, $resource)
    {
        if ($data->hasMorePages()) {
            $custom = collect(['code' => JsonResponse::HTTP_PARTIAL_CONTENT, 'message' => __('repository-generator.partial_content')]);
            $resource = $custom->merge(['data' => $resource]);
            return response()->json($resource, JsonResponse::HTTP_PARTIAL_CONTENT);
        }

        if ($data->isEmpty()) {
            $custom = collect(['code' => JsonResponse::HTTP_REQUESTED_RANGE_NOT_SATISFIABLE, 'message' => __('repository-generator.no_content')]);
            $resource = $custom->merge(['data' => $resource]);
            return response()->json($resource, JsonResponse::HTTP_REQUESTED_RANGE_NOT_SATISFIABLE);
        }

        $custom = collect(['code' => JsonResponse::HTTP_OK, 'message' => __('repository-generator.success')]);
        $resource = $custom->merge(['data' => $resource]);
        return response()->json($resource, JsonResponse::HTTP_OK);
    }
}
