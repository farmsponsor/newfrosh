<?php

namespace App\Interfaces\Carts;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface CartInterface
 * @package App\Interfaces\Carts
 */
interface CartInterface extends ContractInterface
{

}
