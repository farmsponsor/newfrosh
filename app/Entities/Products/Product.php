<?php

namespace App\Entities\Products;

use Shamaseen\Repository\Generator\Utility\Entity;

/**
 * Class Product
 * @package App\Entities
 */
class Product extends Entity
{
    protected $guarded = [];
}
