<?php

namespace App\Http\Resources\Users;

use Shamaseen\Repository\Generator\Utility\JsonResource;
use Shamaseen\Repository\Generator\Utility\Request;

/**
 * Class UserResource
 * @package App\Http\Resources\Users
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'email' => $this->name,
            'created_at' => $this->created_at,
            'token' => $this->createToken('authToken')->accessToken
        ];
    }
}
