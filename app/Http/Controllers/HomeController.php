<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function latestProducts()
    {
        return response()->json(['data' => 'Hello'],200);
    }

    public function bestSellingProducts()
    {
        return response()->json(['data'=> 'Newest'],200);
    }
}
