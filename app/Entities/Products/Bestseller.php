<?php

namespace App\Entities\Products;

use Shamaseen\Repository\Generator\Utility\Entity;

/**
 * Class Bestseller
 * @package App\Entities
 */
class Bestseller extends Entity
{
    protected $guarded = [];
}
