<?php

namespace App\Entities\Carts;

use Shamaseen\Repository\Generator\Utility\Entity;

/**
 * Class Cart
 * @package App\Entities
 */
class Cart extends Entity
{
    protected $guarded = [];
}
