<?php

namespace App\Interfaces\Products;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface BestsellerInterface
 * @package App\Interfaces\Products
 */
interface BestsellerInterface extends ContractInterface
{

}
