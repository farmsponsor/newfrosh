<?php

namespace App\Repositories\Carts;

use App\Entities\Carts\Cart;
use App\Interfaces\Carts\CartInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;

/**
 * Class CartRepository
 * @package App\Repositories\Carts
 * @property-read Cart $model
 */
class CartRepository extends AbstractRepository implements CartInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Cart::class;
    }
}
