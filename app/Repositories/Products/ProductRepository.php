<?php

namespace App\Repositories\Products;

use App\Entities\Products\Product;
use App\Interfaces\Products\ProductInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;

/**
 * Class ProductRepository
 * @package App\Repositories\Products
 * @property-read Product $model
 */
class ProductRepository extends AbstractRepository implements ProductInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Product::class;
    }
}
