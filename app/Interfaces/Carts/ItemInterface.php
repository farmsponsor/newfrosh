<?php

namespace App\Interfaces\Carts;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface ItemInterface
 * @package App\Interfaces\Carts
 */
interface ItemInterface extends ContractInterface
{

}
