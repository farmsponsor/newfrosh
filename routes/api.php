<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['prefix' => 'v1'],function(){

    Route::get('/', function(){
        return 'Hello #ENDSARS';
    });

    Route::post('/register', 'Auth\RegisterController@register');
    Route::post('/login', 'Auth\LoginController@login');

    Route::get('/products', 'ProductController@all');
    Route::get('/products/latest', 'ProductController@latest');
    Route::get('/products/bestsellers', 'ProductController@bestSelling');

    Route::group(['middleware' => 'auth:api'], function(){
        Route::resource('users', 'Users\UserController');
        Route::post('/logout', 'Auth\LoginController@logout');
    });
});


Route::resource('carts', 'Carts\CartController');
Route::resource('items', 'Carts\ItemController');

Route::resource('products', 'Products\ProductController');
Route::resource('bestsellers', 'Products\BestsellerController');
