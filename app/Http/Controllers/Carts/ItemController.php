<?php

namespace App\Http\Controllers\Carts;

use App\Http\Requests\Carts\ItemRequest;
use App\Http\Resources\Carts\ItemResource;
use App\Interfaces\Carts\ItemInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class ItemController
 * @package App\Http\Controllers\Carts
 * @property-read ItemInterface $interface
 */
class ItemController extends Controller
{

    protected $routeIndex = 'items';

    protected $pageTitle = 'Item';
    protected $createRoute = 'items.create';

    protected $viewIndex = 'items.index';
    protected $viewCreate = 'items.create';
    protected $viewEdit = 'items.edit';
    protected $viewShow = 'items.show';

    /**
     * ItemController constructor.
     * @param ItemInterface $interface
     * @param ItemRequest $request
     * @param ItemResource $resource
     */
    public function __construct(ItemInterface $interface, ItemRequest $request)
    {
        parent::__construct($interface, $request, new  ItemResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return parent::store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }
}
