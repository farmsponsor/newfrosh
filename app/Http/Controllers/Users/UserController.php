<?php

namespace App\Http\Controllers\Users;

use App\Http\Requests\Users\UserRequest;
use App\Http\Resources\Users\UserResource;
use App\Interfaces\Users\UserInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class UserController
 * @package App\Http\Controllers\Users
 * @property-read UserInterface $interface
 */
class UserController extends Controller
{

    protected $routeIndex = 'users';

    protected $pageTitle = 'User';
    protected $createRoute = 'users.create';

    protected $viewIndex = 'users.index';
    protected $viewCreate = 'users.create';
    protected $viewEdit = 'users.edit';
    protected $viewShow = 'users.show';

    /**
     * UserController constructor.
     * @param UserInterface $interface
     * @param UserRequest $request
     * @param UserResource $resource
     */
    public function __construct(UserInterface $interface, UserRequest $request)
    {
        parent::__construct($interface, $request, new  UserResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return parent::store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }
}
