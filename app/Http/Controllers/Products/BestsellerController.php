<?php

namespace App\Http\Controllers\Products;

use App\Http\Requests\Products\BestsellerRequest;
use App\Http\Resources\Products\BestsellerResource;
use App\Interfaces\Products\BestsellerInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class BestsellerController
 * @package App\Http\Controllers\Products
 * @property-read BestsellerInterface $interface
 */
class BestsellerController extends Controller
{

    protected $routeIndex = 'bestsellers';

    protected $pageTitle = 'Bestseller';
    protected $createRoute = 'bestsellers.create';

    protected $viewIndex = 'bestsellers.index';
    protected $viewCreate = 'bestsellers.create';
    protected $viewEdit = 'bestsellers.edit';
    protected $viewShow = 'bestsellers.show';

    /**
     * BestsellerController constructor.
     * @param BestsellerInterface $interface
     * @param BestsellerRequest $request
     * @param BestsellerResource $resource
     */
    public function __construct(BestsellerInterface $interface, BestsellerRequest $request)
    {
        parent::__construct($interface, $request, new  BestsellerResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return parent::store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }
}
