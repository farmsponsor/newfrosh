<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BestsellingProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'populate:bestseller';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate the best selling products every 3 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }
}
