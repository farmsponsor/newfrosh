<?php

namespace App\Interfaces\Users;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface UserInterface
 * @package App\Interfaces\Users
 */
interface UserInterface extends ContractInterface
{

}
