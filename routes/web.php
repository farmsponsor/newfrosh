<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::resource('carts', 'Carts\CartController');
Route::resource('items', 'Carts\ItemController');
Route::resource('products', 'Products\ProductController');
Route::resource('bestsellers', 'Products\BestsellerController');