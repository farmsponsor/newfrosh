<?php

namespace App\Entities\Carts;

use Shamaseen\Repository\Generator\Utility\Entity;

/**
 * Class Item
 * @package App\Entities
 */
class Item extends Entity
{
    protected $guarded = [];
}
