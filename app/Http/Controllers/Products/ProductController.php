<?php

namespace App\Http\Controllers\Products;

use App\Http\Requests\Products\ProductRequest;
use App\Http\Resources\Products\ProductResource;
use App\Interfaces\Products\ProductInterface;
use Shamaseen\Repository\Generator\Utility\Controller;

/**
 * Class ProductController
 * @package App\Http\Controllers\Products
 * @property-read ProductInterface $interface
 */
class ProductController extends Controller
{

    protected $routeIndex = 'products';

    protected $pageTitle = 'Product';
    protected $createRoute = 'products.create';

    protected $viewIndex = 'products.index';
    protected $viewCreate = 'products.create';
    protected $viewEdit = 'products.edit';
    protected $viewShow = 'products.show';

    /**
     * ProductController constructor.
     * @param ProductInterface $interface
     * @param ProductRequest $request
     * @param ProductResource $resource
     */
    public function __construct(ProductInterface $interface, ProductRequest $request)
    {
        parent::__construct($interface, $request, new  ProductResource([]));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return parent::index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return parent::create();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        return parent::store();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return parent::show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return parent::edit($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return parent::update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        return parent::destroy($id);
    }
}
