<?php

namespace App\Repositories\Products;

use App\Entities\Products\Bestseller;
use App\Interfaces\Products\BestsellerInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;

/**
 * Class BestsellerRepository
 * @package App\Repositories\Products
 * @property-read Bestseller $model
 */
class BestsellerRepository extends AbstractRepository implements BestsellerInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Bestseller::class;
    }
}
