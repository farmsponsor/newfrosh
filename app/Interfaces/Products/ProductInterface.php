<?php

namespace App\Interfaces\Products;


use Shamaseen\Repository\Generator\Utility\ContractInterface;

/**
 * Interface ProductInterface
 * @package App\Interfaces\Products
 */
interface ProductInterface extends ContractInterface
{

}
