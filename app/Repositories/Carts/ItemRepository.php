<?php

namespace App\Repositories\Carts;

use App\Entities\Carts\Item;
use App\Interfaces\Carts\ItemInterface;
use Shamaseen\Repository\Generator\Utility\AbstractRepository;
use Illuminate\Container\Container as App;

/**
 * Class ItemRepository
 * @package App\Repositories\Carts
 * @property-read Item $model
 */
class ItemRepository extends AbstractRepository implements ItemInterface
{
    protected $with = [];

    /**
     * @param App $app
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Item::class;
    }
}
